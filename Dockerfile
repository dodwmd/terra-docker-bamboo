FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

# Setup useful environment variables
ENV BAMBOO_HOME     /var/atlassian/bamboo
ENV BAMBOO_INSTALL  /opt/atlassian/bamboo
ENV BAMBOO_VERSION  5.14.3.1

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

RUN adduser --system --no-create-home --group --home ${BAMBOO_HOME} --shell /bin/bash bamboo

RUN apt-get update --quiet \
    && apt-get install --quiet --yes --no-install-recommends software-properties-common

# Install Atlassian Bamboo and helper tools and setup initial home
# directory structure.
RUN set -x \
    && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
    && add-apt-repository -y ppa:webupd8team/java \
    && apt-get update --quiet \
    && apt-get install --quiet --yes --no-install-recommends oracle-java8-installer oracle-java8-set-default unzip libtcnative-1 git-core xmlstarlet curl wget \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/* /var/cache/oracle-jdk8-installer \
    && mkdir -p               "${BAMBOO_HOME}/lib" \
    && chmod -R 700           "${BAMBOO_HOME}" \
    && chown -R bamboo:bamboo "${BAMBOO_HOME}" \
    && mkdir -p               "${BAMBOO_INSTALL}" \
    && curl -Ls               "https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-${BAMBOO_VERSION}.tar.gz" | tar -zx --directory  "${BAMBOO_INSTALL}" --strip-components=1 --no-same-owner \
    && curl -Ls                "https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.38.tar.gz" | tar -xz --directory "${BAMBOO_INSTALL}/lib" --strip-components=1 --no-same-owner "mysql-connector-java-5.1.38/mysql-connector-java-5.1.38-bin.jar" \
    && chmod -R 700           "${BAMBOO_INSTALL}/conf" \
    && chmod -R 700           "${BAMBOO_INSTALL}/logs" \
    && chmod -R 700           "${BAMBOO_INSTALL}/temp" \
    && chmod -R 700           "${BAMBOO_INSTALL}/work" \
    && chown -R bamboo:bamboo "${BAMBOO_INSTALL}/conf" \
    && chown -R bamboo:bamboo "${BAMBOO_INSTALL}/logs" \
    && chown -R bamboo:bamboo "${BAMBOO_INSTALL}/temp" \
    && chown -R bamboo:bamboo "${BAMBOO_INSTALL}/work" \
    && sed --in-place         's/^# umask 0027$/umask 0027/g' "${BAMBOO_INSTALL}/bin/setenv.sh" \
    && xmlstarlet             ed --inplace \
        --delete              "Server/Service/Engine/Host/@xmlValidation" \
        --delete              "Server/Service/Engine/Host/@xmlNamespaceAware" \
                              "${BAMBOO_INSTALL}/conf/server.xml" \
    && touch -d "@0"          "${BAMBOO_INSTALL}/conf/server.xml"

COPY "build/entrypoint.sh" "/"
RUN chmod 755 /entrypoint.sh

# Expose default HTTP
EXPOSE 8085

# Set volume mount points for installation and home directory. Changes to the
# home directory needs to be persisted as well as parts of the installation
# directory due to eg. logs.
VOLUME ["/var/atlassian/bamboo","/opt/atlassian/bamboo/logs"]

CMD ["/entrypoint.sh", "/opt/atlassian/bamboo/bin/start-bamboo.sh", "-fg"]

# Set the default user & working directory as the Bamboo home directory.
WORKDIR /var/atlassian/bamboo

ENTRYPOINT ["/entrypoint.sh"]
